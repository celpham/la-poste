FROM python:3.7-alpine
RUN pip install pipenv

WORKDIR /opt/app/

COPY ./Pipfile ./
COPY ./Pipfile.lock ./
RUN set -ex && pipenv install --deploy --system

COPY ./ ./
EXPOSE 5000

CMD [ "gunicorn", "-b0.0.0.0:5000", "--log-level", "info", "app:app" ]