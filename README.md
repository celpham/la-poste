# Technical test

## CELESTIN - ANSWER

#### Solution description

 * unit / integration / E2E tests are to be found in *tests* folder, according to app modules folder structure
 * used flask-testing, unittest, mock, responses, in-memory database for testing (specific Flask Config)
 * La Poste API is in own class app.la_poste.api.LaPosteApi
 * multithreading solution is using flask-executor (which wraps ThreadPoolExecutor)
 * Postman Collection tests -> <a href="https://www.getpostman.com/collections/ff084a7d2e2ee76e1273">my tests</a>
 * added Docker app packaging + Docker-compose to speed up E2E testing
 * added GITLAB CI + build in GITLAB repo (Static code analysis, Unit Testing, Docker Build)
 * I will stop there for this exercice, we could go much further, but it does exceed this exercice context

##### API Description

 * `GET /v1/ping` answer pong
 * `GET /v1/letters/` list all letters in database
 * `GET /v1/letters/<tracking_number>` retrieve letter for given <tracking_number>
 * `POST /v1/letters/<tracking_number>` create letter with <tracking_number> in database
 * `PUT /v1/letters/` update status of all letters in database from La Poste API
 * `PUT /v1/letters/<tracking_number>` update status of <tracking_number> letter in database from La Poste API

#### How to test?
 * `docker-compose up --build` run E2E tests check through docker-compose (no automatic assert for now, human check in docker-compose run)
 * `docker-compose up --build app` start the app, accessible on http://localhost:5000/
 * `pipenv run flask run` run locally the app, accessible on http://localhost:5000/ *requires python 3.7, pipenv, pipenv install*

## EXERCISE STATEMENT

#### Setup
 * Install python 3.7 & pipenv
 * `pipenv install` to install project's requirements
#### Run
 * `pipenv shell` to enter virtual environment (loading the variables in .env)
 * `flask run`
 
#### Explore DB
Database is running on SQLite, it can be browsed using "DB Browser for SQLite" for instance

#### Expected work

1. Connect to La Poste API
2. Create an endpoint that fetch the status of a given letter, and update it in the database
3. Create an endpoint that fetch the status of all letters, and update it in the database
4. Make previous endpoint respond instantly, and launch an asynchronous task that update all the status

There is no need to do a front interface, you can just share a postman collection to test it.

#### Bonus

- Unit, integration, E2E tests
- Store the status history of each letter in a new table
- Impress us