from flask import Blueprint, jsonify, request
from flask_cors import CORS

from app.models.letter import Letter
from app.la_poste.api import LaPosteApi
from app import app, executor


v1 = Blueprint("v1", __name__)
CORS(v1)

@v1.route('/ping', methods=['GET'])
def ep_ping():
    return "pong", 200


@v1.route('/letters/<tracking_number>', methods=['POST'])
def ep_create_letter(tracking_number):
    if not tracking_number:
        return f"Invalid tracking number {tracking_number}", 400

    if Letter.query.filter_by(tracking_number=tracking_number).scalar() is not None:
        return f"Nothing to do : letter object was already created", 200

    letter = Letter()
    letter.tracking_number = tracking_number
    letter.add()
    return f"All done : letter object {letter.id} has been created", 201


@v1.route('/letters/<tracking_number>', methods=['PUT', 'GET'])
def ep_update_letter_status(tracking_number):
    if request.method == "GET":
        letter = Letter.query.filter_by(tracking_number=tracking_number).first()
        if not letter:
            return f"Not Found", 404

        return jsonify(letter.serialize())

    api = LaPosteApi(app.config["LA_POSTE_API_KEY"])
    letter_status = api.get_letter_status(tracking_number)

    if not letter_status:
        return f"Status not found for {tracking_number}", 404

    ep_create_letter(tracking_number)
    letter = Letter.query.filter_by(tracking_number=tracking_number).first()
    letter.status = letter_status
    letter.save()

    return f"Updated {tracking_number} status {letter_status}", 200


@v1.route('/letters/', methods=['PUT', 'GET'])
def ep_update_letter_status_all():
    if request.method == "GET":
        return jsonify(list(map(lambda l: l.serialize(), Letter.query.all())))

    def _process_letters():
        for letter in Letter.query.all():
            message, status_code = ep_update_letter_status(letter.tracking_number)

            if status_code != 200:
                return response

    executor.submit(_process_letters)

    return f"All letter statuses UPDATED", 200
