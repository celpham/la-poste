import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SQLALCHEMY_DATABASE_URI = "sqlite:///test.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LA_POSTE_API_KEY = "EpHLLsjCzpyzQ8yEZzx6CvZXj4sVW0+4m0uAXMU1pTvnI/Hk3n438J7Ux/+YM6IK"
    EXECUTOR_TYPE = "thread"
    EXECUTOR_MAX_WORKERS = "5"


class TestConfig(Config):
    ENV_TYPE = "test"
    TESTING = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
    LA_POSTE_API_KEY = "API_KEY"


class DevelopmentConfig(Config):
    ENV_TYPE = "development"


class ProductionConfig(Config):
    ENV_TYPE = "production"


config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "default": DevelopmentConfig,
}
