import requests

class LaPosteApi:

    def __init__(self, api_key):
        self.api_key = api_key

    def get_letter_status(self, tracking_number):
        response = requests.get(
            f"https://api.laposte.fr/suivi/v2/idships/{tracking_number}?lang=fr_FR",
            headers={
                'Accept': 'application/json',
                'X-Okapi-Key': self.api_key,
            })

        if response.status_code == 400:
            return None

        if response.status_code != 200:
            error_message = response.data if hasattr(response, 'data') else response.status_code
            raise requests.exceptions.ConnectionError(f"La Poste API Error: { error_message }")

        events = response.json()["shipment"]["event"]
        events.sort(key=lambda e: e["order"])
        return events[-1]["code"]
