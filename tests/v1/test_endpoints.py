from flask_testing import TestCase
from app import app, db
from app.config import TestConfig
from app.models.letter import Letter

import time
import mock
from app.la_poste.api import LaPosteApi

class EndpointsV1Tests(TestCase):

    def create_app(self):
        app.config.from_object(TestConfig)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()


    def test_ping_pong(self):
        response = self.client.get("/v1/ping")
        self.assertStatus(response, 200)
        self.assertEqual(b'pong', response.data)


    def test_can_create_letter(self):
        response = self.client.post("/v1/letters/TRACK_NUMBER")
        self.assertStatus(response, 201)
        self.assertEqual(b'All done : letter object 1 has been created', response.data)

        # Assert in database
        letters = Letter.query.all()
        self.assertEqual(1, len(letters))
        self.assertEqual("TRACK_NUMBER", letters[0].tracking_number)
        self.assertEqual(None, letters[0].status)

        # Test case - letter creation can be called multiple times safely 
        response = self.client.post("/v1/letters/TRACK_NUMBER")
        self.assertStatus(response, 200)
        self.assertEqual(b'Nothing to do : letter object was already created', response.data)

        # Assert in database
        letters = Letter.query.all()
        self.assertEqual(1, len(letters))
        self.assertEqual("TRACK_NUMBER", letters[0].tracking_number)
        self.assertEqual(None, letters[0].status)

    def test_update_letter_status(self):
        with mock.patch.object(LaPosteApi, 'get_letter_status', return_value='NEW STATUS'):
            response = self.client.put("/v1/letters/TRACK_NUMBER")
            self.assertStatus(response, 200)
            self.assertEqual(b"Updated TRACK_NUMBER status NEW STATUS", response.data)
            self.assertEqual("NEW STATUS", Letter.query.all()[0].status)

    def test_try_update_letter_status_when_no_status(self):
        with mock.patch.object(LaPosteApi, 'get_letter_status', return_value=None):
            response = self.client.put("/v1/letters/TRACK_NUMBER")
            self.assertStatus(response, 404)
            self.assertEqual(b"Status not found for TRACK_NUMBER", response.data)
            self.assertEqual(0, len(Letter.query.all()), "The letter should not be created in database if no status")

    
    def test_update_all_letters_status(self):
        def _mock_get_letter_status(self, tracking_number):
            return "NEW STATUS"

        for i in range(1, 101):
            self.client.post(f"/v1/letters/LETTER_{i}")        
        
        self.assertEqual(100, len(Letter.query.all()))

        with mock.patch.object(LaPosteApi, 'get_letter_status', autospec=True, side_effect=_mock_get_letter_status):
            elapsed = time.perf_counter()
            response = self.client.put("/v1/letters/")
            elapsed = time.perf_counter() - elapsed
            self.assertStatus(response, 200)
            self.assertEqual(b"All letter statuses UPDATED", response.data)
            self.assertGreater(0.05, elapsed, "Return duration of the method should be instantaneous (<50ms)")

            # Add delay for the multithread processing to finish
            time.sleep(1)
            self.assertFalse(list(filter(lambda l: l.status != "NEW STATUS", Letter.query.all())), 
                "All status should be updated in database")

    def test_get_one_letter(self):
        self.client.post(f"/v1/letters/LETTER")
        response = self.client.get("/v1/letters/LETTER")
        self.assertStatus(response, 200)
        self.assertEqual({"id": 1, "status": None, "tracking_number": "LETTER"}, response.json)

        response = self.client.get("/v1/letters/UNKNOWN_LETTER")
        self.assertStatus(response, 404)

    def test_get_all_letters(self):
        self.client.post(f"/v1/letters/LETTER")
        self.client.post(f"/v1/letters/LETTER2")
        response = self.client.get("/v1/letters/")
        self.assertStatus(response, 200)
        expected_json = [
            {"id": 1, "status": None, "tracking_number": "LETTER"},
            {"id": 2, "status": None, "tracking_number": "LETTER2"},
        ]
        self.assertEqual(expected_json, response.json)
