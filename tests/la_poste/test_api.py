import unittest
import responses
import requests
import json
from app.la_poste.api import LaPosteApi


class LaPosteApiTests(unittest.TestCase):


    @responses.activate
    def test_api_correct_tracking_number(self):
        api = LaPosteApi("TEST_KEY")

        with open('./tests/mock/3C00638102084.json') as json_file:
            responses.add(responses.GET, 
                'https://api.laposte.fr/suivi/v2/idships/3C00638102084?lang=fr_FR',
                json=json.load(json_file),
                headers= {
                    'Accept': 'application/json',
                    'X-Okapi-Key': 'TEST_KEY',
                },
                status=200)

        self.assertEqual("DI2", api.get_letter_status("3C00638102084"))


    @responses.activate
    def test_api_invalid_tracking_number(self):
        api = LaPosteApi("TEST_KEY")
        responses.add(responses.GET, 'https://api.laposte.fr/suivi/v2/idships/3C00638102084?lang=fr_FR', status=400)
        self.assertEqual(None, api.get_letter_status("3C00638102084"))


    @responses.activate
    def test_api_non_authorized(self):
        api = LaPosteApi("TEST_KEY")
        responses.add(responses.GET, 'https://api.laposte.fr/suivi/v2/idships/A?lang=fr_FR', status=401)
                
        with self.assertRaises(requests.exceptions.ConnectionError):
            api.get_letter_status("A")

    @responses.activate
    def test_api_random_error_with_data(self):
        api = LaPosteApi("TEST_KEY")
        responses.add(responses.GET, 'https://api.laposte.fr/suivi/v2/idships/A?lang=fr_FR', 
            status=402, 
            body=b"Error Message")

        with self.assertRaises(requests.exceptions.ConnectionError):
            api.get_letter_status("A")


