#!/bin/sh

echo " ** TEST PING ** "
curl -s -X GET http://app:5000/v1/ping && echo -e "\n"

echo " ** TEST CREATE ONE LETTER ** "
curl -s -X POST http://app:5000/v1/letters/RK633119313NZ
curl -s -X GET http://app:5000/v1/letters/RK633119313NZ && echo -e "\n"

echo " ** TEST UPDATE ONE LETTER ** "
curl -s -X GET http://app:5000/v1/letters/3C00638101995
curl -s -X PUT http://app:5000/v1/letters/3C00638101995 && echo -e "\n"
curl -s -X GET http://app:5000/v1/letters/3C00638101995 && echo -e "\n"

echo " ** TEST UPDATE ALL LETTERS ** "
curl -s -X PUT http://app:5000/v1/letters/
sleep 2
curl -s -X GET http://app:5000/v1/letters/ && echo -e "\n"